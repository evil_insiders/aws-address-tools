#!/usr/bin/env python
__author__ = "Nathan Sipes"
__copyright__ = "Copyright (C) 2017 Nathan Sipes"
__license__ = "Apache License, Version 2.0 http://www.apache.org/licenses/"
__version__ = ".1"

import json
import argparse
from netaddr import *
import codecs
import requests
import re


#load URL with JSON IP Ranges
r = requests.get('https://ip-ranges.amazonaws.com/ip-ranges.json')
#load json into dictionary
ip_json = r.json()

def juniper_srx_address_book_ipv6(region,service, args):
    with codecs.open('{}-{}-ipv4.srx-address.txt'.format(region,service), 'a', encoding='utf-8') as ef:
        for prefix in ip_json['ipv6_prefixes']:
            if prefix['region'] == region:
                if service != 'all':
                    if prefix['service'] == service:
                        reg = prefix['region'].upper()
                        svc = prefix['service'].upper()
                        ipv6 = prefix['ipv6_prefix']
                        ef.write('set security address-book global address ipv6.AWS-{}-{}-{} {}\n'.format(reg,svc.lower(),ipv6,ipv6))
                        ef.write('set security address-book global address-set ipv6.AWS-{}-{} address ipv6.aws-{}-{}-{}\n'.format(reg,svc,reg,svc,ipv6,ipv6))
                    else:
                        pass
    
                elif service == 'all':
                    reg = prefix['region'].upper()
                    svc = prefix['service'].upper()
                    ipv6 = prefix['ipv6_prefix']
                    address, mask = re.split('/', ipv6)
                    ef.write('set security address-book global address ipv6.AWS-{}-{}-{} {}\n'.format(reg,svc,ipv6,ipv6))
                    ef.write('set security address-book global address-set ipv6.AWS-{}-{} address ipv6.aws-{}-{}-{}\n'.format(reg,svc,reg,svc,ipv6,ipv6))
                else:
                    print "skipping"


def juniper_srx_address_book_ipv4(region, service, args):
    with codecs.open('{}-{}-ip.srx-address.txt'.format(region,service), 'a', encoding='utf-8') as ef:
        for prefix in ip_json['prefixes']:
            if prefix['region'] == region:
                if service != 'all':
                    if prefix['service'] == service:
                        reg = prefix['region']
                        svc = prefix['service']
                        ip = prefix['ip_prefix']
                        address, mask = re.split('/', ip)
                        ef.write('set security address-book global address AWS-{}-{}-{} {}\n'.format(reg,svc,address,ip))
                        ef.write('set security address-book global address-set AWS-{}-{} address aws-{}-{}-{}\n'.format(reg,svc,reg,svc,address,ip))
                    else:
                        pass
    
                elif service == 'all':
                    reg = prefix['region']
                    svc = prefix['service']
                    ip = prefix['ip_prefix']
                    address, mask = re.split('/', ip)
                    ef.write('set security address-book global address AWS-{}-{}-{} {}\n'.format(reg,svc,address,ip))
                    ef.write('set security address-book global address-set AWS-{}-{} address aws-{}-{}-{}\n'.format(reg,svc,reg,svc,address,ip))
                else:
                    print "skipping"    
    

        
        
def juniper_prefix_ipv6(region, service, args):
    with codecs.open('{}-{}-ipv6.prefix-list.txt'.format(region,service), 'a', encoding='utf-8') as ef:
        for prefix in ip_json['ipv6_prefixes']:
            if prefix['region'] == region:
                if service != 'all':
                    if prefix['service'] == service:
                        reg = prefix['region']
                        svc = prefix['service']
                        ipv6 = prefix['ipv6_prefix']
                        print('set policy-options prefix-list ipv6.{}-{} {}\n'.format(reg,svc,ipv6))
                        ef.write('set policy-options prefix-list ipv4.{}-{} {}\n'.format(reg,svc,ipv6))
                    else:
                        pass
    
                elif service == 'all':
                    reg = prefix['region']
                    svc = prefix['service']
                    ipv6 = prefix['ipv6_prefix']
                    print('set policy-options prefix-list ipv6.{}-{} {}\n'.format(reg,svc,ipv6))
                    ef.write('set policy-options prefix-list ipv4.{}-{} {}\n'.format(reg,svc,ipv6))
                else:
                    print "skipping"
  

def juniper_prefix_ipv4(region, service, args):
    with codecs.open('{}-{}-ipv4.prefix-list.txt'.format(region,service), 'a', encoding='utf-8') as ef: 
        if region != "all":
            for prefix in ip_json['prefixes']:
                if prefix['region'] == region:
                    if service != 'all':
                        if prefix['service'] == service:
                            reg = prefix['region']
                            ip = prefix['ip_prefix']
                            svc = prefix['service']
                            #print('Found match region:{} Prefix:{} Service:{} \n'.format(prefix['region'],prefix['ip_prefix'],prefix['service']))
                            print('set policy-options prefix-list ipv4.{}-{} {}\n'.format(reg,svc,ip))
                            ef.write('set policy-options prefix-list ipv4.{}-{} {}\n'.format(reg,svc,ip))
                        else:
                            pass
                    elif service == 'all':
                        reg = prefix['region']
                        ip = prefix['ip_prefix']
                        svc = prefix['service']
                        #print('Found match region:{} Prefix:{} Service:{} \n'.format(prefix['region'],prefix['ip_prefix'],prefix['service']))
                        print('set policy-options prefix-list ipv4.{}-{} {}\n'.format(reg,svc,ip))
                        ef.write('set policy-options prefix-list ipv4.{}-{} {}\n'.format(reg,svc,ip))
        

    
def output(region):
    #get ips for specified regions
    #print to specified format
    if args.service is not None:        
        service = args.service
    else:
        service = 'all'
    if args.format == "wireshark":
        print wireshark_format(ip_list)
    elif args.format == "wireshark_redirect":
            print "\"aws_ip_range_%s\" " %(region) + wireshark_format(ip_list)
    elif args.format == "cisco":
        cisco_format(ip_list)
    elif args.format == "srx-address":
        if args.type.lower() == "ipv4":
            juniper_srx_address_book_ipv4(region, service, args)
        elif args.type.lower() == "ipv6":
            juniper_srx_address_book_ipv6(region, service, args)
        else:
            juniper_srx_address_book_ipv4(region, service, args)
            juniper_srx_address_book_ipv6(region, service, args)
        
    elif args.format == "juniper-prefix":
        if args.type.lower() == "ipv4":
            juniper_prefix_ipv4(region, service, args)
        elif args.type.lower() == "ipv6":
            juniper_prefix_ipv6(region,  service, args)
        else:
            juniper_prefix_ipv4(region, service, args)
            juniper_prefix_ipv6(region, service, args)
            
    else:
        print "not a valid format. Please use help to see existing features"



#used to validate user input or loop through all regions
region_list = ['us-east-1', 'us-west-2', 'us-west-1', 'eu-west-1', 'eu-central-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-northeast-1', 'sa-east-1']

#argparse what user wants
parser = argparse.ArgumentParser(description='Obtain AWS IP list and print various formats')
parser.add_argument('--region', help='filter to specific regions, comma seperated. for all, leave out this argument', choices = ['us-east-1', 'us-west-2', 'us-west-1', 'eu-west-1', 'eu-central-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-northeast-1', 'sa-east-1'])
parser.add_argument('--format', help='which format to output', required=True, choices=['wireshark', 'wireshark_redirect', 'cisco', 'srx-address', 'juniper-prefix'])
parser.add_argument('--service', help='Specify the service that you would like to create a list for', choices=['AMAZON','ROUTE53_HEALTHCHECKS','S3','EC2','CLOUDFRONT'])
parser.add_argument('--type',help='Specify the version IPv4 or IPv6 leaving this blank will generate both', choices=['ipv4','ipv6', 'both'], default='both')
args = parser.parse_args()


def main():
#Parse the regions
    if args.region:
        regions = args.region.split(',')
        for region in regions:
            if region in region_list:
                output(region)
            else:
                print "invalid region", region
        
    else:
        region = "all"
        output(region)
   

if __name__ == '__main__':
    main()