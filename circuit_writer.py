#!/usr/bin/env python3

import boto3
import os
import sys
import requests
import json
import time
import argparse
import re
import pickle


raptor_url = 'https://raptor.nms.symcpe.net/'
raptor_key = '97213e12-b7fa-44a7-973f-169fd2cf8ae0'
location_api = '/api/locations'
key = 'bfec8141-9344-4e90-8d9b-6b32a5b22532'
headers = {'X-API-Token':'{}'.format(key)}
raptor_header = {'X-API-Token':'{}'.format(raptor_key)}
url = 'https://ipam-staging.nms.symcpe.net'
default_path = os.path.dirname(os.path.realpath(__file__))
time_str = time.strftime("%Y%m%d-%H")
circuitDict = {}
interfaceDict = {}
deviceDict = {}
deviceByIp = {}
interfaceByIp = {}

regions = ['us-east-1','us-east-2','us-west-1','us-west-2','ca-central-1','eu-west-1','eu-central-1','eu-west-2','ap-northeast-1','ap-northeast-2','ap-southeast-1','ap-southeast-2','ap-south-1','sa-east-1']

# Vendor	Account Number	Identifier	Service Order	A Side	Z Side	Z Side Name	Logical	BGP Key	Description	Install Date	Contract Start Date	Contract End Date	Committed Data Rate	Auto Renewal	Status	Protected	Wave Length	Business Unit 
def build_help():   
    parser = argparse.ArgumentParser(description='Reads a spreadsheet and produces \
                                     configuration for an oob instance on an SRX')
    parser.add_argument('--region', dest='region',action='store', help='Provide me the region you want to build the connections in.')
   
    parser.add_argument('--aws-profile', dest='aws_profile',action='store', help='What set of credentials do you want to use.. Hint you can find the available by looking at the .aws/credentials file in your home directory after the docker image creates it')
    parser.add_argument('--provider',dest='provider',action='store',help='The provider name, aws or azure. Defaults to aws', choices=['aws','azure'],default='aws')
    args = parser.parse_args()
    return args

def build_circuit_dict():
    raptor_url = 'https://raptor.nms.symcpe.net/api/circuits'
    raptor_key = '97213e12-b7fa-44a7-973f-169fd2cf8ae0'
    raptor_header = {'X-API-Token':'{}'.format(raptor_key)}
    circuits = requests.get(raptor_url, headers = raptor_header)
    circuitList = circuits.content.decode("utf-8")
    elements = json.loads(circuitList)
    #https://raptor.nms.symcpe.net/api/circuits/
    for circuit in elements:
        circuitId = circuit['identifier']
        primaryKey = circuit['id'] 
        circuitDict[circuitId] = primaryKey 
        #print(json.dumps(circuit, sort_keys=True, indent=4))
        
    #     print(circuit)
    # print(circuitDict)
    print(circuits.status_code)

def build_device_dict():
    raptor_url = 'https://raptor.nms.symcpe.net/api/devices'
    raptor_key = '97213e12-b7fa-44a7-973f-169fd2cf8ae0'
    raptor_header = {'X-API-Token':'{}'.format(raptor_key),
                 'Accept': 'application/json'}

    devicesList = requests.get(raptor_url, headers = raptor_header)
    decodedList = devicesList.content.decode("utf-8")
    elements = json.loads(decodedList)
    """
    For the list of devices that Raptor returned
    add each device to the worker queue.
    """
    for element in elements:
        devId = element['id']
        hostname = element['hostname']
        deviceDict[hostname] = devId

        
    print(devicesList.status_code)

def build_interface_dict(): 
    raptor_key = '97213e12-b7fa-44a7-973f-169fd2cf8ae0'
    raptor_header = {'X-API-Token':'{}'.format(raptor_key),
                 'Accept': 'application/json'}
    skipIp = re.compile('^12[7-8]')
    for host, primaryKey in deviceDict.items():
        raptor_url = 'https://raptor.nms.symcpe.net/api/devices/' + str(host) + '.json'
        device = requests.get(raptor_url, headers = raptor_header)
        deviceAttributes = device.content.decode("utf-8")
        elements = json.loads(deviceAttributes)
        for interface in elements['interfaces']:
            addresses = interface['addresses']
            for address in addresses:
                if address == '[]':
                    pass
                else:
                    intIp = address
                    #print(address)
                    if skipIp.match(intIp):
                        pass
                    else:
                        intId = interface['id']
                        #print(address)
                        name = interface['name']
                        print("device {} Interface name {} interface id {} interface ip {}".format(host, name, intId, intIp))
                        interfaceDict[intIp] = intId
                        deviceByIp[intIp] = host
                        interfaceByIp[intIp] = name
                        

                
            
            #print(interface)
        #https://raptor.nms.symcpe.net/api/circuits/
    #     for circuit in elements:
    #         circuitId = circuit['identifier']
    #         primaryKey = circuit['id'] 
    #         circuitDict[circuitId] = primaryKey 
    #         #print(json.dumps(circuit, sort_keys=True, indent=4))
    #         
    # #     print(circuit)
    # # print(circuitDict)
    # print(circuits.status_code)

    
# devicesList = requests.get(raptor_url, headers = raptor_header, verify=False)
#     deviceIds = {}
#     decodedList = devicesList.content.decode("utf-8")
#     elements = json.loads(decodedList)
#     """
#     For the list of devices that Raptor returned
#     add each device to the worker queue.
#     """
#     for element in elements:
#         devId = element['id']
#         hostname = element['hostname']
#         deviceIds[hostname] = devId
#     

def aws_get_existing_vifs(region, args, **kwargs):
    aws_profile = args.aws_profile
    session = boto3.Session(profile_name=aws_profile, region_name=region)
    client = session.client('directconnect')

    try:    
        aws_virtual_interfaces = client.describe_virtual_interfaces()
        if aws_virtual_interfaces is not None:  
            for interface in aws_virtual_interfaces.get('virtualInterfaces'):
                yield(interface)                
        else:
            return
    except botocore.exceptions.ClientError as botoerror:
        print('{} {}'.format(aws_profile_name, botoerror))


        
def aws_get_existing_connections(region, args, **kwargs):
    aws_profile = args.aws_profile
    session = boto3.Session(profile_name=aws_profile, region_name=region)
    client = session.client('directconnect')
    connections = client.describe_connections()
    try:
        if connections is not None:
            for connection in connections.get('connections'):
                yield(connection)
        else:
            return
    except:
        print("Error")
        pass


def raptor_get_devices():
    raptor_url = 'https://raptor.nms.symcpe.net/api/circuits'
    raptor_key = '97213e12-b7fa-44a7-973f-169fd2cf8ae0'
    raptor_header = {'X-API-Token':'{}'.format(raptor_key)}
    device = requests.get(raptor_url, headers = raptor_header, json=data, verify=False)
    
    print(device.status_code)
    return     
  
def circuits_writer(data, method, circuitId):
    raptor_url = 'https://raptor.nms.symcpe.net/api/circuits'
    raptor_key = '97213e12-b7fa-44a7-973f-169fd2cf8ae0'
    raptor_header = {'X-API-Token':'{}'.format(raptor_key)}
    if method == 'post':
        post_raptor = requests.post(raptor_url, headers = raptor_header, json=data)
        print(post_raptor.status_code)
        return post_raptor
    elif method == 'patch':
        post_raptor = requests.patch(raptor_url + "/" + str(circuitId) , headers = raptor_header, json=data)
        print(post_raptor.status_code)
        return post_raptor
    else:
        return 'error'
        
    
    return post_raptor

    
def main():
    args = build_help()
    build_circuit_dict()
    if os.path.isfile("deviceByIp.p"):
        deviceDict = pickle.load(open("deviceDict.p","rb"))
        interfaceByIp = pickle.load(open("interfaceByIp.p","rb"))
        interfaceDict = pickle.load(open("interfaceDict.p","rb"))
        deviceByIp = pickle.load(open("deviceByIp.p","rb"))
        
    else:
        build_device_dict()
        build_interface_dict()
        pickle.dump(interfaceByIp, open("interfaceByIp.p",'wb'))
        pickle.dump(interfaceDict, open("interfaceDict.p",'wb'))
        pickle.dump(deviceDict, open("deviceDict.p",'wb'))
        pickle.dump(deviceByIp, open("deviceByIp.p",'wb'))
    if args.region is None:
        print("working on all regions")
        for region in regions:
            print('current region is:{}'.format(region))
            connections = aws_get_existing_connections(region, args)
            for connection in connections:
                if connection.get('bandwidth') == '10Gbps':
                    rate = 10000
                elif connection.get('bandwidth') == '1Gbps':
                    rate = 1000
                else:
                    pass
                directConnect = connection.get('connectionId')
                if directConnect in circuitDict:
                    pass
                else:
                    print("Direct Connect information: {}/n".format(connection))
                    data={'circuit':{'vendor_id':92}}
                    #data['circuit'].append({'vendor_id':'92'})
                    data['circuit'].update({'account_number':connection.get('ownerAccount')})
                    data['circuit'].update({'identifier':connection.get('connectionId')})
                    #data['circuit'].append({'service_order':connection.get('ownerAccount')})
                    #data['circuit'].append({'a_side':connection.get('ownerAccount')})
                    data['circuit'].update({'z_side':connection.get('awsDevice')})
                    data['circuit'].update({'z_side_name':connection.get('region')})
                    data['circuit'].update({'logical':0})
                    data['circuit'].update({'description':connection.get('connectionName')})
                    # data['circuit'].append({'install_date':connection.get('ownerAccount')})
                    # data['circuit'].append({'contract_start_date':connection.get('ownerAccount')})
                    data['circuit'].update({'status':connection.get('connectionState')})
                    if connection.get('bandwidth') == '10Gbps':
                        data['circuit'].update({'committed_data_rate':10000})
                    elif connection.get('bandwidth') == '1Gbps':
                        data['circuit'].update({'committed_data_rate':1000})
                    else:
                        pass
                    post = circuits_writer(data, 'post')
    
            vifs = aws_get_existing_vifs(region, args)
            for vif in vifs:
                vifId = vif.get('virtualInterfaceId')
                
                if vifId in circuitDict:
                    cpeIp, mask = re.split('/',vif.get('customerAddress'))
                    parentId = vif.get('connectionId')
                    raptorCircuitId = circuitDict.get(parentId)
                    intKey = interfaceDict.get(cpeIp)
                    host = deviceByIp.get(cpeIp)
                    interface = interfaceByIp.get(cpeIp)
                    vifData={'circuit':{'vendor_id':92}}
                    vifData['circuit'].update({'parent_id':circuitDict.get(parentId,"")})
                    vifData['circuit'].update({'account_number':vif.get('ownerAccount')})
                    vifData['circuit'].update({'identifier':vif.get('virtualInterfaceId')})
                    vifData['circuit'].update({'service_order':vif.get('ownerAccount')})
                    vifData['circuit'].update({'a_side':'VLAN:{}'.format(vif.get('vlan'))})
                    vifData['circuit'].update({'z_side':vif.get('virtualGatewayId')})
                    vifData['circuit'].update({'z_side_name':vif.get('location')})
                    vifData['circuit'].update({'logical':1})
                    vifData['circuit'].update({'description':vif.get('virtualInterfaceName')})
                    vifData['circuit'].update({'source_interface_id':intKey})
                    vifData['circuit'].update({'bgp_key':vif.get('authKey')})
                    vifData['circuit'].update({'status':vif.get('virtualInterfaceState')})
                    circuitId = circuitDict.get(vifid)
                    print('vif:{} is :{} and already exists\n'.format(vifId, circuitId))
                    post = circuits_writer(vifData, 'patch', circuitId)
                    print(post.status_code)
                    pretty = json.dumps(vifData, indent=1)
                    print(pretty)
                else:
                    print(vif)
                    cpeIp, mask = re.split('/',vif.get('customerAddress'))
                    parentId = vif.get('connectionId')
                    raptorCircuitId = circuitDict.get(parentId)
                    intKey = interfaceDict.get(cpeIp)
                    host = deviceByIp.get(cpeIp)
                    interface = interfaceByIp.get(cpeIp)
                    #print("cpeIp: {} Result from Raptor {}".format(cpeIp,intKey))
                    vifData={'circuit':{'vendor_id':92}}
                    vifData['circuit'].update({'parent_id':circuitDict.get(parentId,"")})
                    vifData['circuit'].update({'account_number':vif.get('ownerAccount')})
                    vifData['circuit'].update({'identifier':vif.get('virtualInterfaceId')})
                    vifData['circuit'].update({'service_order':vif.get('ownerAccount')})
                    vifData['circuit'].update({'a_side':'VLAN:{}'.format(vif.get('vlan'))})
                    vifData['circuit'].update({'z_side':vif.get('virtualGatewayId')})
                    vifData['circuit'].update({'z_side_name':vif.get('location')})
                    vifData['circuit'].update({'logical':1})
                    vifData['circuit'].update({'description':vif.get('virtualInterfaceName')})
                    #vifData['circuit'].update({'install_date':vif.get('ownerAccount')})
                    vifData['circuit'].update({'source_interface_id':intKey})
                    vifData['circuit'].update({'bgp_key':vif.get('authKey')})
                    vifData['circuit'].update({'status':vif.get('virtualInterfaceState')})
                    # if vif.get('bandwidth') == '10Gbps':
                    #     vifData['circuit'].update({'committed_data_rate':10000})
                    # elif vif.get('bandwidth') == '1Gbps':
                    #     vifData['circuit'].update({'committed_data_rate':1000})
                    # else:
                    #     pass
                    circuitId = ''
                    post = circuits_writer(vifData, 'post')
                    print(post.status_code)
                    pretty = json.dumps(vifData, indent=1)
                    print(pretty)
                    #print(type(data))
                    #print(pretty)        
                    """    
                    vif format {'ownerAccount': '427885296256',
                    'virtualInterfaceId': 'dxvif-fgkhipl3',
                    'location': 'EQAM3',
                    'connectionId': 'dxcon-fg20b790',
                    'virtualInterfaceType': 'private',
                    'virtualInterfaceName': 'cpe-ams3-bcr2-xe-4/2/6-ps-prod01-vi',
                    'vlan': 112,
                    'asn': 8283038,
                    'authKey': 'cpe-bgp-key-210575',
                    'amazonAddress': '100.126.255.119/31',
                    'customerAddress': '100.126.255.118/31',
                    'addressFamily': 'ipv4',
                    'virtualInterfaceState': 'available',
                    
                    'customerRouterConfig': '<?xml version="1.0" encoding="UTF-8"?>\n<logical_connection id="dxvif-fgkhipl3">\n  <vlan>112</vlan>\n
                    <customer_address>100.126.255.118/31</customer_address>\n  <amazon_address>100.126.255.119/31</amazon_address>\n
                    <bgp_asn>8283038</bgp_asn>\n  <bgp_auth_key>cpe-bgp-key-210575</bgp_auth_key>\n  <amazon_bgp_asn>7224</amazon_bgp_asn>\n
                    <connection_type>private</connection_type>\n</logical_connection>\n',
                    
                    'virtualGatewayId': 'vgw-09d55a39',
                    'routeFilterPrefixes': [],
                    'bgpPeers': [{'asn': 8283038,
                    'authKey': 'cpe-bgp-key-210575',
                    'addressFamily': 'ipv4',
                    'amazonAddress': '100.126.255.119/31',
                    'customerAddress': '100.126.255.118/31',
                    'bgpPeerState': 'available',
                    'bgpStatus': 'up'}]}
                    """

    else:
        region = args.region.lower()
        
        if region in regions:
            print('Region is:{}'.format(region))
            connections = aws_get_existing_connections(region, args)
            for connection in connections:
                print(connection)
                #a, b, c, d, e, f = re.split('-,',connection.get('connectionName'))
                data={'circuit':{'vendor_id':92}}
                #data['circuit'].append({'vendor_id':'92'})
                data['circuit'].update({'account_number':connection.get('ownerAccount')})
                data['circuit'].update({'identifier':connection.get('connectionId')})
                #data['circuit'].append({'service_order':connection.get('ownerAccount')})
                #data['circuit'].append({'a_side':connection.get('ownerAccount')})
                data['circuit'].update({'z_side':connection.get('awsDevice')})
                data['circuit'].update({'z_side_name':connection.get('region')})
                data['circuit'].update({'logical':0})
                data['circuit'].update({'description':connection.get('connectionName')})
                # data['circuit'].append({'install_date':connection.get('ownerAccount')})
                # data['circuit'].append({'contract_start_date':connection.get('ownerAccount')})
                data['circuit'].update({'status':connection.get('connectionState')})
                if connection.get('bandwidth') == '10Gbps':
                    data['circuit'].update({'committed_data_rate':10000})
                elif connection.get('bandwidth') == '1Gbps':
                    data['circuit'].update({'committed_data_rate':1000})
                else:
                    pass
                
                # data['circuit'].append({'account_number':connection.get('ownerAccount')})
    
                pretty = json.dumps(data, indent=1)
                print(pretty)
                print(type(data))
               # post = circuits_writer(data)
                print(post.status_code)
                print(pretty)
        else:
            print('You entered an invalid region')
            
    
            # {'ownerAccount': '172119256206', 'connectionId': 'dxcon-ffzcq9dl', 'connectionName': 'cpe-ash4-bcr2-xe-4/2/6 ', 'connectionState': 'available', 'region': 'us-east-1', 'location': 'EqDC2', 'bandwidth': '10Gbps', 'loaIssueTime': datetime.datetime(2016, 11, 14, 11, 52, 48, tzinfo=tzlocal()), 'awsDevice': 'EqDC2-tulbue1k57ud'}

if __name__ == '__main__':
    main()    
